set nocompatible              " required
filetype off                  " required

" set the runtime path to include Vundle and initialize
set rtp+=~/.vim/bundle/Vundle.vim
call vundle#begin()

" alternatively, pass a path where Vundle should install plugins
"call vundle#begin('~/some/path/here')

" let Vundle manage Vundle, required
Plugin 'gmarik/Vundle.vim'

" add all your plugins here (note older versions of Vundle
" used Bundle instead of Plugin)

" ایندنت خوب اتوماتیک
Plugin 'vim-scripts/indentpython.vim'


" YouCompleteMe
" apt install build-essential cmake vim python3-dev
" cd ~/.vim/bundle/YouCompleteMe
" python3 install.py --all
Bundle 'Valloric/YouCompleteMe'

" چک کردن سینتکس
Plugin 'vim-syntastic/syntastic'
Plugin 'nvie/vim-flake8'

" nerdtree
Plugin 'scrooloose/nerdtree'
Plugin 'jistr/vim-nerdtree-tabs'

" powerline
Plugin 'Lokaltog/powerline', {'rtp': 'powerline/bindings/vim/'}

" debuger
Plugin 'vim-vdebug/vdebug'

"auto save
Plugin '907th/vim-auto-save'
" ...

" All of your Plugins must be added before the following line
call vundle#end()            " required
filetype plugin indent on    " required

" Enable folding (instead za -> space)
set foldmethod=indent
set foldlevel=99

set encoding=utf-8
set number
set showmatch
set smartcase
set incsearch
set history=1000
set undolevels=1000
set wildignore=*.swp,*.bak,*.pyc
set visualbell
set noerrorbells
set mouse=a
set nocompatible
syntax on
set ruler
set showcmd
set incsearch
set hlsearch
set backspace=indent,eol,start



" فرمت بهتر فایل برای پایتون
au BufNewFile,BufRead *.py
    \set tabstop=4
    \set softtabstop=4
    \set shiftwidth=4
    \set textwidth=79
    \set expandtab
    \set autoindent
    \set fileformat=unix

" BadWhitespace
au BufRead,BufNewFile *.py,*.pyw,*.c,*.h match BadWhitespace /\s\+$/
highlight BadWhitespace ctermbg=red guibg=darkred

"python with virtualenv support
"py << EOF
"import os
"import sys
"if 'VIRTUAL_ENV' in os.environ:
"  project_base_dir = os.environ['VIRTUAL_ENV']
"  activate_this = os.path.join(project_base_dir, 'bin/activate_this.py')
"  execfile(activate_this, dict(__file__=activate_this))
"vEOF

" چک کردن سینتکس
let python_highlight_all=1
syntax on

" colorscheme
colorscheme desert

" nerdtree
let NERDTreeIgnore=['\.pyc$', '\~$'] "ignore files in NERDTree

" ycm config
let g:ycm_autoclose_preview_window_after_completion=1

let g:auto_save = 1
map g  :YcmCompleter GoToDefinitionElseDeclaration


